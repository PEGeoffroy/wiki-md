# Wiki-MD

Little project to generate wiki based on a single page md file.

***

Using :

* Lite-server
* SASS
* concurrently

***

```sh
npm install
npm run all
```

It will launch:

* A sass watcher
* A lite server

To minify the SASS lauch:

```sh
npm run sass-minify
```
