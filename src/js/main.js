"use strict";

let main = document.getElementById("main");
let home = document.getElementById("home");

let wikis = ["rolegame"];

/**
 * Return async data
 * @return {data} The result return data
 */
async function asyncCall(name) {
    let response = await fetch(`./src/data/${name}.md`);
    let data = await response.text();
    return data;
};

/**
 * Display main page
 */
function displayHome() {
    wikis.forEach(character => {
        let p = createElement("p", character, "tab");
        p.addEventListener("click", () => {
            asyncCall(character)
                .then(data => {
                    var lines = data.split('\n');
                    for(var line = 0; line < lines.length; line++){
                        if (lines[line].length > 0) {
                            let type = lines[line].split(' ')[0];
                            if (type === "#") {
                                main.appendChild(createElement("h1", lines[line].replace("# ", "")));
                            } else if (type === "##"){
                                main.appendChild(createElement("h2", lines[line].replace("## ", "")));
                            } else if (type === "*"){
                                let i = 0
                                let list = document.createElement("ul");
                                while (lines[line + i][0] === "*" && lines[line + i][0] !== undefined) {
                                    list.appendChild(createElement("li", lines[line + i].replace("* ", "")));
                                    i++;
                                }
                                line += i;
                                main.appendChild(list);
                            } else {
                                main.appendChild(createElement("p", lines[line]));
                            }
                        }
                    }
                })
        }, false);
        main.appendChild(p);
    });
}

/**
 * Create an HTML element
 * @param {string} type The type of HTML element we want to create (h*, p).
 * @param {string} text its content.
 * @param {string} addClass to possibly add a class.
 * @return {HTMLElement} The result return an HTML element with the content
 */
function createElement(type, text, addClass = "") {
    let element = document.createElement(type);
    element.innerHTML = text;
    if (addClass !== "") element.setAttribute("class", `${addClass}`);
    return element;
}

/**
 * Clean the main
 */
function clean() {
    main.innerHTML = "";
}

/**
 * Switch page
 */
function switchPage(page = "menu") {
    main.className = "";
    main.setAttribute("class", page);
}

/**
 * Create a FA icon
 * @param {string} name the name of FA icon.
 * @param {string} addition has a specification (s, b, ...) ?
 * @param {int} size to increase icon size.
 * @return {HTMLElement} The result return a p with icon inside
 */
function addIcon(name, addition = "", size = 1, surrounded = true) {
    let i = document.createElement("i");
    i.setAttribute("class", `fa${addition} fa-${size}x fa-${name}`);

    if (surrounded) {
        let p = document.createElement("p");
        p.setAttribute("class", "icon");
        p.appendChild(i);
        return p;
    }
    return i;
}

displayHome();